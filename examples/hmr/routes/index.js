const Router = require('koa-router');
const db = require('./db');

const shopRouter = new Router();
const petsRouter = new Router();

petsRouter.get('/pets', (ctx, next) => {
  const names = Object.keys(db);
  ctx.body = 'pets: ' + names.join(', ');
});
petsRouter.get('/pets/:name', (ctx, next) => {
  const pet = db[ctx.params.name];
  if (!pet) return ctx.throw('cannot find a pet by name ' + ctx.params.name, 404);
  ctx.body = pet.name + ' is a ' + pet.species;
});
shopRouter.use('/shop', petsRouter.routes(), petsRouter.allowedMethods());

module.exports = shopRouter.routes();
