const Koa = require('koa');
const chokidar = process.env.NODE_ENV !== 'production' ? require('chokidar') : null;

if (chokidar) {
  const watcher = chokidar.watch('./routes');
  watcher.on('ready', () => {
    watcher.on('all', () => {
      Object.keys(require.cache).forEach((id) => {
        if(id.indexOf('routes') !== -1) console.log('keys', id);
        if (/[\/\\]routes[\/\\]/.test(id)) {
          delete require.cache[id];
          app.use(async (ctx, next) => require('./routes')(ctx, next));
        }
      });
    });
  });
}

const app = new Koa();

app.use(async (ctx, next) => require('./routes')(ctx, next));

app.listen(3000);
console.log('listening on port 3000');
