# Server project with WebSockets, Cluster and HMR

* Web server: Koa
* WebSockets: WS
* Cluster: node native, how to do a failover?
* HMR: watcher via chokidar