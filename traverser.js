const middlewares = [];

middlewares.push(async (ctx, next) => {
  console.log('first, down');
  await next();
  console.log('first, up');
});

middlewares.push(async (ctx, next) => {
  console.log('second, down');
  await next();
  console.log('second, up');
});

middlewares.push(async (ctx, next) => {
  console.log('third, down');
  await next();
  console.log('third, up');
});

const createContext = (req, res) => ({ req, res });

const callback = () => {
  const fn = compose(middlewares);

  const localHandleRequest = (req, res) => {
    const ctx = createContext(req, res);
    return handleRequest(ctx, fn);
  };

  return localHandleRequest;
};

const handleRequest = (ctx, fnMiddleware) => {
  const res = ctx.res;
  res.statusCode = 404;
  const onerror = err => console.error(err);
  const handleResponse = () => console.log(ctx);
  console.log('Finished');
  return fnMiddleware(ctx).then(handleResponse).catch(onerror);
};

const compose = (middleware) => {
  if (!Array.isArray(middleware)) throw new TypeError('Middleware stack must be an array!')
  for (const fn of middleware) {
    if (typeof fn !== 'function') throw new TypeError('Middleware must be composed of functions!')
  }

  return function (context, next) {
    // last called middleware #
    let index = -1;
    console.log('index', index);
    return dispatch(0)
    function dispatch (i) {
      console.log('dispatch', i, i === middleware.length, next);
      if (i <= index) return Promise.reject(new Error('next() called multiple times'))
      index = i
      let fn = middleware[i]
      if (i === middleware.length) fn = next
      if (!fn) return Promise.resolve()
      try {
        return Promise.resolve(fn(context, dispatch.bind(null, i + 1)));
      } catch (err) {
        return Promise.reject(err)
      }
    }
  }
}

callback()({ type: 'req' }, { type: 'res' });